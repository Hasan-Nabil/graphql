import { ClassRow } from "./class.model";
import { StudentModel } from "../orm";
import { StudentResolver } from "../student/student.resolver";

export const ClassResolver = {
    student: (classroom: ClassRow) => {
        return StudentModel.findAll({where: {classId: classroom.id}})
    }
}