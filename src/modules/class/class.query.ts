import { ClassModel } from "../orm";

export async function getClassQuery(root, {name}, ctx) {
    return await ClassModel.findOne({ where: {name: name} })
}