import { Instance, Sequelize, DataTypes } from "sequelize";
// import { db } from "../../lib/connector/postgres";

export const model = 'class';

export interface ClassRow {
    id: number;
    name: string;
}

export function defineClassModel (db: Sequelize, {STRING, INTEGER}: DataTypes) {
    return db.define<Instance<ClassRow>, ClassRow>(
        model, {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name: STRING
        },
        {
            timestamps: false,
            freezeTableName: true
        }
    );
}