import { readSchema } from "../../lib";
import { ClassResolver } from "./class.resolver";
import { getClassQuery } from "./class.query";


const schema = readSchema('./class.gql', __dirname);

export const schemas = [
    schema
];

export const typeResolvers = {
    Class: ClassResolver
}

export const queryResolvers = {
    class: getClassQuery
}
