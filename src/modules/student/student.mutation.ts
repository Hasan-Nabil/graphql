import { StudentModel } from "../orm";

export async function addStudentMutation (
    root,
    {form},
    ctx
) {
    return StudentModel.create(form);
}

export async function editStudentMutation (
    root,
    {id, form},
    ctx
) {
    await StudentModel.update(form, { where: {id : id} })

    if(!id) return;
    return await StudentModel.findOne({
        where: {id}
    }); 

}

export async function deleteStudentMutation ( root, {id}, ctx ) {
    return await StudentModel.destroy({ where: { id:id } })
}