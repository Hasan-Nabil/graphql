import { StudentModel } from "../orm";

export async function getStudentQuery (
    root,
    {id}: {id: number},
    ctx
) {
    if(!id) return;
    return StudentModel.findOne({
        where: {id}
    })
}

export async function getAllStudentQuery (
    root,
    { where },
    ctx
) {
    // const { classId } = where;
    // return StudentModel.findAll({
    //     where: { classId }
    // })

    const filter = () => {
        let filter = {};
        if (where) {
            if (where.classId) {
                Object.assign(filter, {classId: where.classId})
            }
        }
        return filter;
    }

    return StudentModel.findAll({
        where: filter()
    })
}