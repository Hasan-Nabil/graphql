import { Instance, Sequelize, DataTypes } from "sequelize";
// import { db } from "../../lib/connector/postgres";

export const model = 'student';

export interface StudentRow {
    id: number;
    name: string;
    classId: number;
}

export function defineStudentModel (db: Sequelize, {STRING, INTEGER}: DataTypes) {
    return db.define<Instance<StudentRow>, StudentRow>(
        model, {
            id: {
                type: INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            name: STRING,
            classId: INTEGER
        },
        {
            timestamps: false,
            freezeTableName: true
        }
    );
}