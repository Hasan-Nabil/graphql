import { StudentRow } from "./student.model";
import { ClassModel } from "../orm";

export const StudentResolver = {
    class: (student: StudentRow) => {
        return ClassModel.findOne({ where:{
            id: student.classId
        }})
    }
}