import { readSchema } from "../../lib";
import { getStudentQuery, getAllStudentQuery } from "./student.query";
import { StudentResolver } from "./student.resolver";
import { addStudentMutation, editStudentMutation, deleteStudentMutation } from "./student.mutation";

const schema = readSchema('./student.gql', __dirname);

export const schemas = [
    schema
];

export const mutationResolvers = {
    addStudent: addStudentMutation,
    editStudent: editStudentMutation,
    deleteStudent: deleteStudentMutation
}

export const typeResolver = {
    Student: StudentResolver
}

export const queryResolvers = {
    student: getStudentQuery,
    students: getAllStudentQuery
}