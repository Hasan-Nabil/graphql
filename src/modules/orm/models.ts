import { db } from '../../lib';
import { SyncOptions, DropOptions } from 'sequelize';
import { model as classModel, defineClassModel } from "../class/class.model";
import { model as studentModel, defineStudentModel } from "../student/student.model";

// Register Model
export const ClassModel = db.import(classModel, defineClassModel);
export const StudentModel = db.import(studentModel, defineStudentModel);

/**
 * Sync database with models defined in ORM
 *
 * @export
 * @param {SyncOptions} [options] sychronization options, like drop table
 * @returns {Promise<void>}
 */
export function syncDatabase(options?: SyncOptions): Promise<void> {
  return new Promise((resolve, reject) => {
    db.sync(options).then(resolve).catch(reject);
  });
}

/**
 * Reset database (drop related table)
 *
 * @export
 * @param {DropOptions} [options] drop options
 * @returns {Promise<void>}
 */
export function clearDatabase(options?: DropOptions): Promise<void> {
  return new Promise((resolve, reject) => {
    db.drop(options).then(resolve).catch(reject);
  });
}

/**
 * Close database connection
 *
 * @export
 * @returns {Promise<void>}
 */
export function closeDatabase(): Promise<void> {
  return new Promise((resolve, reject) => {
    db.close().then(resolve).catch(reject);
  });
}

export default db;
