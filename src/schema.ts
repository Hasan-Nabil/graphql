import { makeExecutableSchema } from 'graphql-tools';
import { readSchema } from './lib';
import { pingQuery } from './modules/ping/ping';
import { schemas as studentSchemas, mutationResolvers as studentMutationResolvers, typeResolver as studentTypeResolvers, queryResolvers as studentQueryResolvers } from "./modules/student";
import { schemas as classSchema, typeResolvers as classTypeResolvers, queryResolvers as classQueryResolvers } from "./modules/class"

export const rootSchema = readSchema('./root.gql', __dirname);

export const graphQLSchema = makeExecutableSchema({
  typeDefs: [
    rootSchema,
    ...studentSchemas,
    ...classSchema
  ],
  resolvers: {
    Query: {
      ping: pingQuery,
      ...studentQueryResolvers,
      ...classQueryResolvers
    },
    Mutation: {
      ...studentMutationResolvers,
    },
    ...studentTypeResolvers,
    ...classTypeResolvers
  }
});
